# Experiencia con RTFM y LMGTFY

No recuerdo la última vez, pero hace un tiempo estaba trabajando con Scrapy
y un compañero tenía un error en la función `parse`, algo con la respuesta 
del `start_request`. Él me pide ayuda y el error era muy específico en cuanto 
a que el problema, cuando vi que era el error lo que hice fue que le envié una 
URL de Google donde se buscaba el error y en las primeras respuestas estaba 
stackoverflow donde lo resolvían. Evidentemente no busco antes de.

**Sistema Operativo:** Windows y un WSL de Ubuntu.

**Lenguajes que domino:**
- Python
- Java
- R

# Ejemplo de automatización de CI para microservicios en python

## Pseudocódigo:

### Inicio del programa:

1. **Descargar los últimos cambios del repositorio:**
   - Clonar el repositorio donde está el código del servicio.
   - Actualizar el repositorio local.

2. **Pruebas unitarias y de integración:**
   - Para cada servicio en el repositorio:
     - Ir a la carpeta del microservicio.
     - Ejecutar pruebas.
     - Validar si todas las pruebas pasaron con exito.
     - Si alguna prueba falla, detener el proceso y revisar.

3. **Construir y empaquetar los servicios:**
   - Para cada servicio en el repositorio:
     - Ir a la carpeta del servicio.
     - Construir el contenedor Docker del servicio.
     - Empaquetar el contenedor Docker y subirlo por ejemplo a Docker Hub.

4. **Desplegar los microservicios en entornos dev:**
   - Utilizar Docker Compose o Kubernetes para desplegar los servicios.
   - Ejecutar pruebas de aceptación para validar la funcionalidad de los servicios.

### Fin del programa.

## Explicación de conceptos:

- **Pruebas unitarias y de integración:** Las pruebas unitarias lo que hacen es 
revisar pedacitos pequeños de código, mientras que las pruebas de integración
miran cómo funcionan juntos los diferentes pedacitos. Ambas son importants
para asegurarnos de que la app funcione bien.

- **Construcción y empaquetado de microservicios:** Aquí, básicamente creamos 
cajitas para cada servicio por así decirlo. Así, podemos llevarlos a 
cualquier lado sin problemas.

- **Despliegue en un entorno de prueba:** Se deben desplegar los servicios 
en embientes de desarrollo para poder así realizar las pruebas, todas estas 
deben ser lo más parecidas a lo que se ve en un ambiente de producción. 
Se pueden utilizar herramientas como Docker Compose o Kubernetes.


**Microservicio de Scraping Web**

Este código es parte de un servicio desarrollado en Python 
que realiza scraping en una página web. El servicio fue creado 
como parte de un proyecto en el que colaboré durante mi empleo anterior. 
El código completo contiene información sensible de la empresa,
entonces he decidido compartir solo unas partes del código de 
scraping para ilustrar mi experiencia en este campo.

Por esta razón no agregaré el código en la imagen Docker, sin embargo,
igualmente dejo las respectivas pruebas de los metodos.

Puedes revisar el código aquí:

[Descargar código](scrapyproject.py)

# Ejercicio de pares coincidentes

**Notas**

He creado el codigo con sus respectivas pruebas con pytest, adicional a esto
se ha probado con flake8 y se a aplicado PEP8 en el desarrollo. He tenido unos
problemas con Docker pues no tengo la virtualización configurada en la BIOS,
sin embargo he creado la imagen para poder hacer su prueba.

[Revisión de la solución](pares)