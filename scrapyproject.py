def parse(self, response, **kwargs):
        """Parses the response from the main URL.

        This method extracts necessary information from the response,
        such as cookies, request verification token, and generates
        an anti-captcha token using the 'generate_token_anticaptcha'
        method. It then constructs a new FormRequest to the main URL
        with the required form data and headers.

        This method relies on various other methods and properties such
        as '_extract_cookies', 'generate_token_anticaptcha', and
        'jsonUserData'. These methods and properties must exist for this
        method to function correctly.

        The FormRequest object contains the main URL, required form data,
        headers, and cookies. The dictionary with the error message will
        be returned in case of an exception or if the request limit is
        exceeded.

        Args:
            response (HtmlResponse): The response object containing
                the HTML content. It should be the response from a
                specific request.

        Yields:
            Union[FormRequest, dict]: A FormRequest object to continue
            the scraping process or a dictionary containing an error
            message in case of an exception. Exceptions that
            could trigger this include TypeError.

         Raises:
            CloseSpider: Raised when the retry limit is reached, closing
            the spider.

        Example::

            # Sample usage within a Spider's parse method
            from my_module import DianFESpider

            def parse(self, response):
                for result in self.parse(response):
                    yield result

        Version: 1.0 - 2023-11-03 15:31
        """
        self.retries_count += 1
        info(f"Attempt # {self.retries_count}.")
        if self.retries_count > 3:
            final_output = {
                self.general_constants.INFOPROJECTS:
                    self.constants.PROCESS_ERROR["error"]
            }
            info("Maximum number of retries exceeded, closing spider.")
            yield final_output
            raise CloseSpider()

        try:
            # Extract cookies from the response
            cookies = self._extract_cookies(response)

            # Extract request verification token from the response
            request_verification_token = response.xpath(
                self.constants.XPATH["RequestVerificationToken"]
            ).get()

            # Generate anti-captcha token
            recaptcha_token = self.generate_token_anticaptcha()

            # Extract document key from jsonUserData
            document_key = self.jsonUserData.get("DocumentKey")

            form_data = {
                "__RequestVerificationToken": request_verification_token,
                "RecaptchaToken": recaptcha_token,
                "DocumentKey": document_key
            }

            # Construct and yield FormRequest
            yield FormRequest(
                url=self.mainUrl,
                formdata=form_data,
                headers=self.constants.HEADERS,
                cookies=cookies,
                callback=self.handle_form_response
            )
        except TypeError:
            final_output = {
                self.general_constants.INFOPROJECTS:
                    self.constants.PROCESS_ERROR["error"]
            }
            yield final_output

    # --------------------------------------------------------------------

    def handle_form_response(self, response: HtmlResponse):
        """Handles the response from the form submission.

        This method checks if the response URL is the main URL. If it is,
        it extracts an error message from the response. If the error
        message matches a predefined error, it yields a dictionary with an
        error message. Otherwise, it makes a new Request to the main URL
        with the 'parse' method as the callback. If the response URL is
        not the main URL, it yields the results of the 'parse_filter' method.

        This method depends on the 'parse' and 'parse_filter' methods, as well as
        several properties and constants. These methods and properties must exist
        for this method to work correctly.

        The Request object contains the main URL and the 'parse' method as the callback.
        The dictionary with the error message will be yielded if an error occurs during
        form submission. The results of the 'parse_filter' method will be yielded if
        the response URL is not the main URL.

        Args:
            response (HtmlResponse): The response object from the form submission.
                It should contain the HTML content of the page after the form
                has been submitted.

        Yields:
            Union[Request, dict]: A Request object to continue the scraping process,
            a dictionary containing an error message in case of an error, or the results
            of the 'parse_filter' method.

        Example::

            # Sample usage within a Spider's handle_form_response method
            from my_module import DianFESpider

            def handle_form_response(self, response):
                for result in self.handle_form_response(response):
                    yield result

        Version: 1.0 - 2023-11-03 15:31
        """
        if response.url == self.mainUrl:
            error_message = response.xpath(
                self.constants.XPATH["span_error"]).get()
            if (
                error_message ==
                self.constants.PROCESS_ERROR["error_page"]['message']
            ):
                final_output = {
                    self.general_constants.INFOPROJECTS:
                        self.constants.PROCESS_ERROR["error_page"]
                }
                yield final_output
            else:
                yield Request(
                    url=self.mainUrl, callback=self.parse, dont_filter=True)
        else:
            yield from self.parse_filter(response)

    # --------------------------------------------------------------------

    def parse_filter(self, response: HtmlResponse):
        """Parses the HTML response to extract filtered data.

        This method extracts data from the HTML response, specifically
        focusing on filtering relevant information. It utilizes XPath
        queries to locate and retrieve the desired elements from the HTML.

        This method relies on various other methods, such as
        '_extract_table_html' and '_extract_table_data'. These methods
        must exist for this method to function correctly.

        The list of dictionaries contains filtered data from the HTML
        response. A ValueError exception will be raised if an error
        occurs during the parsing process.

        Args:
            response (HtmlResponse): The HTML response from the web page.
            It should be the response from a specific request.

        Returns:
            List[Dict[str, Any]]: A list of dictionaries containing
            filtered data. Each dictionary represents a set of filtered
            information.

        Raises:
            ValueError: If the parsing process encounters unexpected
                issues or the response structure is not as expected.

        Version: 1.0 - 2023-11-03 15:31
        """
        table_html = self._extract_table_html(response)
        output_dict = self._extract_table_data(table_html)
        final_output = {self.general_constants.INFOPROJECTS: output_dict}
        yield final_output

    # -------------------------------------------------------------------------

    def _extract_cookies(self, response: HtmlResponse) -> Union[str, Dict]:
        """Extracts cookies from the response.

        Args:
            response (HtmlResponse): The response object containing
                the HTML content.

        Returns:
            dict: A dictionary containing the extracted cookies.

        Raises:
            ValueError: If there is an issue with extracting cookies.

        Version: 1.0 - 2023-11-03 15:31
        """
        try:
            cookies = response.headers.getlist('Set-Cookie')
            cookies_format = {}
            for cookie in cookies:
                cookie_str = cookie.decode('utf-8')
                key, value = cookie_str.split('=', 1)
                cookies_format[key] = value.split(';')[0]
            return cookies_format

        except Exception:
            # Handle exceptions during the request and yield error message
            final_output = self.constants.PROCESS_ERROR["error"]
            return final_output

    # -------------------------------------------------------------------------

    def _extract_table_html(self, response: HtmlResponse) -> Union[str, Dict]:
        """Extracts the HTML content of the table from the response.

        This method extracts the HTML content of a specific table from
        the response. It uses an XPath query to locate and retrieve the
        table content.

        This method depends on the constant 'XPATH["table"]'. This
        constant must exist for this method to function correctly.

        The HTML content of the table is returned as a text string. A
        ValueError exception will be raised if an error occurs during
        the extraction process.

        Args:
            response (HtmlResponse): The HTML response from the web page.
                It should be the response from a specific request.

        Returns:
            Union[str, Dict]: The HTML content of the table as a text
                string, or a dictionary with an error message if an
                exception occurs.

        Raises:
            ValueError: If there is an issue extracting the HTML from
                the table.

        Version: 1.0 - 2023-11-03 15:31
        """
        try:
            table_html = response.xpath(
                self.constants.XPATH["table"]
            ).extract_first()

            return table_html
        except Exception:
            # Handle exceptions during the request and yield error message
            final_output = self.constants.PROCESS_ERROR["error"]
            return final_output

    # -------------------------------------------------------------------------

    def _extract_table_data(self, table_content: str) -> List[Dict]:
        """Extracts data from the table of the provided HTML content.

        This method uses BeautifulSoup to parse the HTML content of the
        table, selects all rows in the table, and extracts the data from
        each column.

        It creates a dictionary for each row with the extracted data and
        adds them to a list.

        This method depends on the constants
        'PROCESS_ERROR["table_error"]' and 'PROCESS_ERROR["error"]'.
        These constants must exist for this method to function correctly.

        The dictionary contains the extracted data from the HTML table.
        A ValueError exception will be raised if an error occurs during
        the extraction process.

        Args:
            table_content (str): The HTML content of the table. It must
                be a text string representing the HTML content of a table.

        Returns:
            dict: A dictionary containing the extracted data from the
                table. Each key of the dictionary represents the name
                of a table column, and its associated value is the data
                extracted from that column.

        Raises:
            ValueError: If there is an issue extracting data.

        Version: 1.0 - 2023-11-03 15:31
        """
        try:
            soup = BeautifulSoup(table_content, 'html.parser')
            rows = soup.select('table.documents-table tbody tr')
            table_error = soup.select('div.alert.panel-footer-grey')
            if table_error:
                return self.constants.PROCESS_ERROR["table_error"]

            data_list = []
            for row in rows:
                columns = row.find_all('td')
                # Extract data from each column and create a dictionary
                data_dict = {
                    'Codigo': columns[0].text.strip(),
                    'Descripcion': columns[1].text.strip(),
                    'Fecha': columns[2].text.strip(),
                    'Nit Emisor': columns[3].text.strip(),
                    'Emisor': columns[4].text.strip(),
                    'Nit Receptor': columns[5].text.strip(),
                    'Receptor': columns[6].text.strip(),
                }
                data_list.append(data_dict)
            return data_list
        except Exception:
            final_output = self.constants.PROCESS_ERROR["error"]
            return final_output





#############################################################################
        
def test_parse(self, caplog):
        spider = DianFESpider(**self.params)

        response = HtmlResponse(
            url=MAIN_URL,
            body='''
                <html>
                    <body>
                        <input name="__RequestVerificationToken" 
                        type="hidden" value="test_verification_token">
                    </body>
                </html>
            '''.encode()
        )

        spider.jsonUserData = {"DocumentKey": "123456789"}

        with mock.patch.object(
            spider,
            "_extract_cookies",
            return_value="test_cookies"
        ), mock.patch.object(
            spider, "generate_token_anticaptcha",
            return_value="test_anticaptcha_token"
        ):
            results = list(spider.parse(response=response))
            form_data = parse_qs(results[0].body.decode('utf-8'))

            assert isinstance(results[0], FormRequest)
            assert form_data == {
                "__RequestVerificationToken": ["test_verification_token"],
                "RecaptchaToken": ["test_anticaptcha_token"],
                "DocumentKey": ["123456789"]
            }

            headers = {
                k.decode('utf-8'): v[0].decode('utf-8')
                for k, v in results[0].headers.items()
            }
            assert headers == spider.constants.HEADERS
            assert results[0].cookies == "test_cookies"

        # ---------------------------------------------------------------------

        spider = DianFESpider(**self.params)
        response = HtmlResponse(
            url=MAIN_URL,
            body='''
                <html>
                    <body>
                        <input name="_RequestVerificationToken" 
                        type="hidden" value="test_verification_token">
                    </body>
                </html>
            '''.encode()
        )

        spider.jsonUserData = {"DocumentKey": "123456789"}

        with mock.patch.object(
            spider,
            "_extract_cookies",
            return_value="test_cookies"
        ), mock.patch.object(
            spider,
            "generate_token_anticaptcha",
            return_value="test_anticaptcha_token"
        ):
            results = list(spider.parse(response=response))
            assert PROCESS_ERROR == results[0]['DATA']

        # ---------------------------------------------------------------------

        # Max attempts retries exceeded
        params = {
            "jsonUserData": dumps({
                "DocumentKey": "123456789"
            })
        }
        spider = DianFESpider(**params)
        spider.retries_count = 3
        with pytest.raises(CloseSpider):
            list(spider.parse(MagicMock()))
        assert spider.retries_count > 3

    # -------------------------------------------------------------------------

    def test_parse_filter(self):
        spider = DianFESpider(**self.params)
        return_value_extract_data = VALID_OUTPUT
        response = HtmlResponse(
            url=MAIN_URL,
            body=TABLE_HTML
        )

        with mock.patch.object(
           spider, "_extract_table_html",
           return_value=RESPONSE_TABLE_HTML
        ), mock.patch.object(
            spider,
            "_extract_table_data",
            return_value=return_value_extract_data
        ):
            results = list(spider.parse_filter(response=response))
            data = [{'DATA': VALID_OUTPUT}]
            data_result = data[0]['DATA']

            assert results[0] == {
                spider.general_constants.INFOPROJECTS: data_result
            }

    # -------------------------------------------------------------------------

    def test_handle_form_response_correct_response(self, dianfe_spider):
        resp = next(dianfe_spider.handle_form_response(
            HtmlResponse(url='dian.gov', body=TABLE_HTML)))

        expected = {
            'DATA': [
                {
                    CODIGO: '030',
                    DESCRIPCION: EVENTO_ACUSE,
                    FECHA: '2022-09-15',
                    NIT_EMISOR: '830054539',
                    EMISOR: 'P.A. VIVA LAURELES',
                    NIT_RECEPTOR: '890900608',
                    RECEPTOR: ALMACENES_EXITO
                }, {
                    CODIGO: '032',
                    DESCRIPCION: EVENTO_RECIBO,
                    FECHA: '2022-09-26',
                    NIT_EMISOR: '830054539',
                    EMISOR: 'P.A. VIVA LAURELES',
                    NIT_RECEPTOR: '890900608',
                    RECEPTOR: ALMACENES_EXITO
                }
            ]
        }
        assert isinstance(resp, dict)
        assert resp == expected

    # -------------------------------------------------------------------------

    def test_handle_form_response_not_found(self, dianfe_spider):
        body = (
            '<form id="search-document-form">'
            '<div class="form-group">'
            '<span class="field-validation-error ">'
            'Documento no encontrado en los registros de la DIAN.'
            '</span>'
            '</div>'
            '</form>'
        )
        resp = next(
            dianfe_spider.handle_form_response(
                HtmlResponse(url=dianfe_spider.mainUrl, body=body.encode())
            )
        )
        value_expected = {
            'DATA': {
                'message':
                    'Documento no encontrado en los registros de la DIAN.'
            }
        }
        assert resp == value_expected