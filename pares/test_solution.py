import pytest # noqa

from solution import encontrar_par


def test_encontrar_par():
    # Par que coincide
    coleccion1 = [2, 3, 6, 7]
    suma_objetivo1 = 9
    assert encontrar_par(coleccion1, suma_objetivo1) == (3, 6)

    # Par que no coincide
    coleccion2 = [1, 3, 3, 7]
    suma_objetivo2 = 9
    assert encontrar_par(coleccion2, suma_objetivo2) == (
        "No se encontró par coincidente"
    )
