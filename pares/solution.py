def encontrar_par(array, suma):
    nums = {}

    for i, num in enumerate(array):
        comp = suma - num
        if comp in nums:
            return (nums[comp], num)
        nums[num] = num

    return "No se encontró par coincidente"
